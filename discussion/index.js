// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log ( 'Addition operator: ' + sum);

let difference = y - x;
console.log('Subtraction operator: ' + difference);

let product = x * y;
console.log('Multiplication operator: ' + product);

let quotient = x + y;
console.log('Division operator: ' + quotient);

let remainder = y % x;
console.log('Modulo operator: ' + remainder);

// Assignment Operator

let assignmentNumber = 8;

assignmentNumber += 2;
console.log ('Addition assignment:' + assignmentNumber);

assignmentNumber += 2;
console.log ('Addition assignment:' + assignmentNumber);

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);

assignmentNumber -= 2;
console.log ('Addition assignment:' + assignmentNumber);

assignmentNumber *= 2;
console.log ('Addition assignment:' + assignmentNumber);

assignmentNumber /= 2;
console.log ('Addition assignment:' + assignmentNumber);


// Multiple Operators and Parenthesis
let mdas = 1+2-3*4/5;
console.log("MDAS: " + mdas);

let pemdas = 1+(2-3)*(4/5);
console.log('PEMDAS: ' + pemdas);

// Increment and Decrement
let z = 1;

let increment = ++z;
console.log("Pre-increment: " + increment);

console.log("Value of z: " + z);

increment = z++;
console.log("Post-increment: " + increment);

console.log("Value of z: " + z);

let decrement = --z;
console.log("Pre-increment: " + decrement);

console.log("Value of z: " + z);

decrement = z--;
console.log("Post-increment: " + decrement);

console.log("Value of z: " + z);

// Type Coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);


let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Equality Operator (==)
console.log('Equality operator:')
console.log (1==1);
console.log (1==2);
console.log(1=='1');
console.log(false == 0);
console.log('johnny' == 'johnny');
console.log('Johnny' == 'johnny');

// Inequality Operator
console.log('Inequality operator:')
console.log (1!=1);
console.log (1!=2);
console.log(1!='1');
console.log(false != 0);
console.log('johnny' != 'johnny');
console.log('Johnny' != 'johnny');

// Strict Equality Operator
console.log('Strict Equality operator:')
console.log (1===1);
console.log (1===2);
console.log(1==='1');
console.log(false === 0);
console.log('johnny' === 'johnny');
console.log('Johnny' === 'johnny');

let johnny = 'john';
console.log('john' === johnny);

// Strict Inequality

console.log('Strict Inquality operator:')
console.log (1!==1);
console.log (1!==2);
console.log(1!=='1');
console.log(false !== 0);
console.log('johnny' !== 'johnny');
console.log('Johnny' !== 'johnny');

let john = 'johnny';
console.log('johnny' !== john);

// Relational Operator

console.log('Relational Operator operator:')

let a = 50;
let b = 65;

let isGreaterThan = a>b;
console.log(isGreaterThan);

let isLessThan = a<b;
console.log(isLessThan);

let isGTE = a>=b;
console.log(isGTE);

let isLTE = a<=b;
console.log(isLTE);

let numStr = '30';
console.log(a>=numStr);

let str = 'twenty';
console.log(b >= str);

// Logical operator

console.log('Logical operator:');
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered
console.log(authorization1);

let authorization2 = isLegalAge && isRegistered
console.log(authorization2);

let authorization3 = isLegalAge && isAdmin
console.log(authorization3);

let random = isAdmin && false;
console.log(random);

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel ===25;
console.log(authorization4);

let authorization5 = isRegistered && isLegalAge && requiredLevel ===95;
console.log(authorization5);

let userName = 'gamer2022';
let userName2 = 'theTinker';
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.lenght > 8 && userAge >=requiredAge;
console.log(registration1);

let registration2 = userName2.lenght > 8 && userAge2 >=requiredAge;
console.log(registration2);


let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >=requiredAge
console.log(guildRequirement);

let guildRequirement2 = isRegistered || userLevel || userLevel2 >= requiredLeve2 >= requiredLevel || userAge2>= requiredAge
console.log (guildRequirement2);

let guildRequirement3 userLevel || userAge >= userAge >= requiredAge;
console.log(guildRequirement3);














